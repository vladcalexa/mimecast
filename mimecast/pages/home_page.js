var home_page = function() {
	
	
	
	this.refreshButton = function(){
		return $('.icon-arrows-ccw');
	};
	
	this.email = function(){
		return $('[ng-click="navbarCtrl.showPopUp($event)"]:nth-child(2)');
	}
	
	this.clickCompose = function(){
		$('[ng-show="mainActionVisible"]').click();
	}
	
	this.compose = function(){
		return $('[ng-show="mainActionVisible"]');
	}
	
	this.enterRecipient = function(text){
		$$('[placeholder="Internal domain recipients only"]').first().click();
		 $$('[placeholder="Internal domain recipients only"]').first().sendKeys(text);
		 browser.actions().sendKeys(protractor.Key.ENTER).perform();
	};
	this.enterSubject = function(text){
		element(by.model('email.subject')).click();
		element(by.model('email.subject')).sendKeys(text);
		
	};
	
	this.emailText = function(text){
		$$('.note-editable').first().click();
		$$('.note-editable').first().sendKeys(text);
	};
	
	this.confirmEmail = function(){
		return $('.growl-message');
	};
	
	this.clickSend = function(){
		$('.icon-paper-plane').click();
	}
};
module.exports  = new home_page();