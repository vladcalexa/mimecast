var forgot_page = function() {
	
	this.nevermind = function(){
		
		var nevermind=$('[translate="FORGOT_PASSWORD_GO_TO_LOGIN_LNK"]');
		return nevermind;
	}
	
	this.resetButton = function(){
		
		var reset=$('[translate="FORGOT_PASSWORD_RESET_PASSWORD_BTN"]');
		return reset;
	}
	
	this.forgottenEmail = function(){
		
		var email = element(by.model('forgotPasswordControllerCtrl.username'));
		return email;
	}
	
	
	this.forgotUrl = function(){
		
		
		return 'https://login-alpha.mimecast.com/m/secure/login/#/forgot-password';
	}
	
	this.password  = function(){
		return element(by.id('password'));
	
	};
	
};
module.exports  = new forgot_page();