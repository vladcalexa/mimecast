var login_page = function() {
	
	this.nextButton = function(){
		var nextButton = $$('.btn-lg').first();
		return nextButton;
	}
	this.userName  = function(text){
		var username = element(by.id('username'));
		return username;
	};
	
	this.enterUserName  = function(text){
		element(by.id('username')).sendKeys(text);
	
	};
	
	this.password  = function(){
		return element(by.id('password'));
	
	};
	this.enterPassword  = function(text){
		element(by.id('password')).sendKeys(text);
	
	};
	
	this.dropDownList =  function(){
		
		return element(by.id('type'));
	}
	this.passwordError  = function(){
		var error = element(by.binding('appCtrl.errorMessage'));
		return error;
	
	};
	this.forgot = function(){
		
		var forgot=$('[translate="LINK_FORGOT_PASSWORD"]');
		return forgot;
	}
	
	this.nextDisabled = function(){
	if( $('.btn-primary').getAttribute('disabled') === 'disabled') {
		return true
	}
	}
	
	this.dropList  = function(){
		var list = element(by.id('type'));
		return list;
	};
	
	
	this.diffUser = function(){
		var different=$('[translate="LINK_LOGIN_AS_DIFFERENT_USER"]');
		return different;
	}
};
module.exports  = new login_page();