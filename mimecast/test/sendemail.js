var login_page = require('../pages/login_page.js');
var home_page = require('../pages/home_page.js');
var testData= require('../test.json');

describe('login functionality', function() {
	
	it('should login with valid credentials', function(){
		browser.get(testData.urlToken);
		browser.driver.manage().window().maximize();
		login_page.enterUserName(testData.username);
		login_page.nextButton().click();
		login_page.enterPassword(testData.password);
		login_page.nextButton().click();
		expect(home_page.refreshButton().isDisplayed()).toEqual(true);
		expect(home_page.email().getText()).toEqual(testData.username);
	});
	
	it('should send an email', function(){
		browser.get(testData.urlToken);
		browser.driver.manage().window().maximize();
		browser.driver.manage().window().maximize();
		login_page.enterUserName(testData.username);
		login_page.nextButton().click();
		login_page.enterPassword(testData.password);
		login_page.nextButton().click();
		browser.wait(() => home_page.compose().isPresent(), 3000);
		home_page.clickCompose();
		
		home_page.enterRecipient('mcasali@mimecast.com');
		home_page.enterSubject('test subject');
		home_page.emailText('test body');
		home_page.clickSend();
		expect(home_page.confirmEmail().isPresent()).toEqual(true);
		
	});
	
});