var login_page = require('../pages/login_page.js');
var forgot_page = require('../pages/forgot_page.js');
var testData= require('../test.json');

describe('login functionality', function() {
	var testData= require('../test.json');//for security purposes a json will be used to provide credentials
	
	beforeEach(function() {
        browser.get(testData.urlSimple);
		browser.driver.manage().window().maximize();
    });
	
	//Test Case1
	it('should have the next button disabled until valid password is entered' , function(){
		expect(login_page.userName().isDisplayed()).toEqual(true);
		expect(login_page.nextButton().getAttribute('disabled')).toBe('true');
		login_page.enterUserName('test@test.com');
		expect(login_page.nextButton().getAttribute('disabled')).not.toBe('true');
	});
	
	//Test Case2.1 
	it('should contain dropdown list and password field when valid user is enetered' , function(){
		login_page.enterUserName(testData.invalidusername);
		login_page.nextButton().click();
		expect(login_page.dropDownList().getText()).toContain("Domain");
		expect(login_page.password().isDisplayed()).toEqual(true);
		expect(login_page.diffUser().getText()).toEqual('Log in as a different user.');
		expect(login_page.forgot().getText()).toEqual('Forgot your password?');
		login_page.diffUser().click();
		expect(login_page.userName().getText()).toEqual("");
	});
	
	//Test Case 2.2
	it('should contain dropdown list and password field when valid user is entered' , function(){
		login_page.enterUserName(testData.invalidusername);
		login_page.nextButton().click();
		login_page.forgot().click();
		var currentUrl = browser.getCurrentUrl();
	    expect(currentUrl).toEqual(forgot_page.forgotUrl());
		expect(forgot_page.resetButton().isDisplayed()).toEqual(true);
		expect(forgot_page.forgottenEmail().isDisplayed()).toEqual(true);
		forgot_page.nevermind().click();
		expect(login_page.password().isDisplayed()).toEqual(true);
		var currentUrl = browser.getCurrentUrl();
		expect(currentUrl).toEqual(testData.urlSimple);
		
	});
	
	//Test Case3
	it('should not login with invalid username' , function(){
		login_page.enterUserName(testData.username);
		login_page.nextButton().click();
		login_page.enterPassword(testData.invalidpassword);
		login_page.nextButton().click();
		expect(login_page.passwordError().getText()).toBe('Invalid user name, password or permissions. Please check you are using the correct URL.');
	});
	
	//Test Case4
	it('should not login with invalid password' , function(){
		login_page.enterUserName(testData.invalidusername);
		login_page.nextButton().click();
		login_page.enterPassword(testData.password);
		login_page.nextButton().click();
		expect(login_page.passwordError().getText()).toBe('Invalid user name, password or permissions. Please check you are using the correct URL.');
	});
  
});